<html>
<head>
<style>
.mainmenubtn {
    background-color: red;
    color: white;
    border: none;
    cursor: pointer;
    padding:20px;
    margin-top:20px;
}
.dropdown {
    position: relative;
    display: inline-block;
}
.dropdown-child {
    display: none;
    background-color: black;
    min-width: 200px;
}
.dropdown-child a {
    color: white;
    padding: 20px;
    text-decoration: none;
    display: block;
}
.dropdown:hover .dropdown-child {
    display: block;
}
</style>
</head>
<body>
<div class="dropdown">
  <button class="mainmenubtn">Main menu</button>
  <div class="dropdown-child">
    <a href="tampil.php">Data Transaksi</a>
    <a href="kamar.php">Data Kamar</a>
    <a href="tamu.php">Data Tamu</a>
    <a href="pegawai.php">Data Pegawai</a>
  </div>
</div>
</body>
</html>