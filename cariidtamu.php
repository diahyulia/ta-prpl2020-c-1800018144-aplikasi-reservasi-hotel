<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Penginapan Nusantara</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/rangeslider.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>
  <body>
  
  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    
    <header class="site-navbar" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-11 col-xl-2">
            <h1 class="mb-0 site-logo"><a href="index.php" class="text-white h2 mb-0">Penginapan Nusantara</a></h1>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block">
                <li><a href="index.php"><span>Home</span></a></li>
                <li class="has-children">
                  <a href="seluruhdata.php"><span>Kumpulan Data</span></a>
                  <ul class="dropdown arrow-top">
                    <li><a href="tampil.php">Data Transaksi</a></li>
                    <li><a href="pegawai.php">Data Pegawai</a></li>
                    <li class="active"><a href="tamu.php">Data Tamu</a></li>
                    <li><a href="kamar.php">Data Kamar</a></li>
                  </ul>
                </li>
                <li><a href="pesan.php"><span>Transaksi Pemesanan Kamar</span></a></li>
                <li><a href="laporanbulan.php"><span>Laporan</span></a></li>
              </ul>
            </nav>
          </div>


          <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>
          <div style="text-align: left; color: #F0F8FF">
            <br><br>
            <form method="post" enctype="multipart/form-data">
            <div class="form-group row mt-2">
                    <div class="col-sm-10">
                      <input type="text" name="keyword" class="form-control" id="">
                    </div>
                    <button type="submit" class="btn btn-sm btn-success" name="cari">Cari</button>
                  </div>
            </form>
            
                <center><h2>Data Tamu</h2>
<table border="3" style="background-color: black">
    <tr>
    	<th>Id Tamu</th>
        <th>Nama</th>
        <th>Alamat</th>
        <th>Jenis Kelamin</th>
        <th>No Telp</th>
        <th>Id Pegawai</th>
        <th>Option</th>
    </tr>
    <?php 
    include "connect.php";
    	if(isset($_POST["cari"])){
    		$search = $_POST['keyword'];

    		$query = $koneksi->query("SELECT * FROM tamu WHERE idTamu LIKE '%$search%'");
                            } else {
                                $query = $koneksi->query("SELECT * FROM tamu");
                            }

                            $no = 1;

                            while($data = mysqli_fetch_assoc($query)) {
	?>
		<tr>
			<td><?php echo $data['idTamu']; ?></td>
			<td><?php echo $data['nama']; ?></td>
			<td><?php echo $data['alamat']; ?></td>
			<td><?php echo $data['jenis_kelamin']; ?></td>
			<td><?php echo $data['notelp']; ?></td>
			<td><?php echo $data['idPegawai'] ?></td>
			<td>
				
				<a class="edit" href="edit3.php?idTamu=<?php echo $data['idTamu']; ?>">Edit</a>
				|<a class="hapus" href="hapustamu.php?idTamu=<?php echo $data['idTamu']; ?>" >Hapus</a>
			</td>
		</tr>
		<?php } ?>
	</table>
  </center>
  <br>
    <p align="left"><a href="cariidtamu.php"><li>Mencari Data tamu berdasarkan Id tamu</li></a></p>
    <p align="left"><a href="carinamatamu.php"><li>Mencari Data tamu berdasarkan nama Tamu</li></a></p>
    <p align="left"><a href="carialamattamu.php"><li>Mencari Data tamu berdasarkan alamat Tamu</li></a></p>
    <p align="left"><a href="carinotamu.php"><li>Mencari Data tamu berdasarkan nomor telepon Tamu</li></a></p>
    <p align="left"><a href="caripegawaitamu.php"><li>Mencari Data tamu berdasarkan Id pegawai yang melayani tamu</li></a></p>
              </div>
    </header>
  

    <div class="site-blocks-cover overlay" style="background-image: url(images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          </div>
        </div>
      </div>
    </div>  

    <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>
          
              
    </header>

  

    <div class="site-blocks-cover overlay" style="background-image: url(images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          </div>
        </div>
      </div>
    </div>     
    
      
    
    
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/rangeslider.min.js"></script>
  

  <script src="js/typed.js"></script>
            <script>
            var typed = new Typed('.typed-words', {
            strings: ["Attractions"," Events"," Hotels", " Restaurants"],
            typeSpeed: 80,
            backSpeed: 80,
            backDelay: 4000,
            startDelay: 1000,
            loop: true,
            showCursor: true
            });
            </script>

  <script src="js/main.js"></script>
  
  </body>
</html>