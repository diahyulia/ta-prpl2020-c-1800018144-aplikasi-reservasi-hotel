<!DOCTYPE html>
<html lang="en">
  <head>
    <title>BeautyView Resort System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/rangeslider.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>
  <body>
  
  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    
    <header class="site-navbar" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-11 col-xl-2">
            <h1 class="mb-0 site-logo"><a href="index.php" class="text-white h2 mb-0">BeautyView Resort System</a></h1>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block">
                <li><a href="index.php"><span>Home</span></a></li>
                <li class="has-children">
                  <a href="seluruhdata.php"><span>Kumpulan Data</span></a>
                  <ul class="dropdown arrow-top">
                    <li class="active"><a href="tampil.php">Data Transaksi</a></li>
                    <li><a href="pegawai.php">Data Pegawai</a></li>
                    <li><a href="tamu.php">Data Tamu</a></li>
                    <li><a href="kamar.php">Data Kamar</a></li>
                  </ul>
                </li>
                <li><a href="tambahtran1.php"><span>Transaksi Pemesanan Kamar</span></a></li>
                <li><a href="laporanbulan.php"><span>Laporan</span></a></li>
              </ul>
            </nav>
          </div>


          <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>
          <div style="text-align: left; color: #F0F8FF">
            <br><br>
                <center><h2>Data Transaksi</h2>
<table border="1" style="background-color: black">
    <tr>
    	<th><center>Id Transaksi</center></th>
        <th><center>Id Tamu</center></th>
        <th><center>No Kamar</center></th>
        <th><center>Waktu Check In</center></th>
        <th><center>Waktu Check Out</center></th>
        <th><center>Sub Total</center></th>
        <th><center>Option</center></th>
    </tr>
    <?php  

		include "connect.php";
		$koneksi = mysqli_query($koneksi,"SELECT * FROM transaksi");
		$no = 1;
		foreach ($koneksi as $data) {
	?>
		<tr>
			<td><?php echo $data['idtransaksi']; ?></td>
			<td><?php echo $data['idTamu']; ?></td>
			<td><?php echo $data['nokamar']; ?></td>
			<td><?php echo $data['checkin']; ?></td>
			<td><?php echo $data['checkout']; ?></td>
			<td><?php echo $data['subtotal'] ?></td>
			<td>
				
				<a class="edit" href="edittran.php?idtransaksi=<?php echo $data['idtransaksi']; ?>">Edit</a>
				|<a class="hapus" href="hapus.php?idtransaksi=<?php echo $data['idtransaksi']; ?>">Hapus</a>
			</td>
		</tr>
		<?php } ?>
	</table>
  <!--<p><a href="tambahtran1.php">Tambah</a></p>-->
</center>
              </div>
    </header>

  

    <div class="site-blocks-cover overlay" style="background-image: url(images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          </div>
        </div>
      </div>
    </div>  

    
    
      
    
    <div class="site-section" data-aos="fade">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center border-primary">
            <h2 class="font-weight-light text-primary">Tipe Kamar yang Tersedia</h2>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 mb-4 mb-lg-4 col-lg-4">
            
            <div class="listing-item">
              <div class="listing-image">
                <img src="images/img_1.jpg" alt="Free Website Template by Free-Template.co" class="img-fluid">
              </div>
              <div class="listing-item-content">
                <a href="listings-single.html" class="bookmark" data-toggle="tooltip" data-placement="left" title="Bookmark"><span class="icon-heart"></span></a>
                <a class="px-3 mb-3 category" href="#">Tipe 1</a>
                <h2 class="mb-1"><a href="listings-single.html">Tempat tidur<br>AC<br>Lemari<br>TV</a></h2>
                <span class="address">Weda, Halmahera Tengah</span>
              </div>
            </div>

          </div>
          <div class="col-md-6 mb-4 mb-lg-4 col-lg-4">
            
            <div class="listing-item">
              <div class="listing-image">
                <img src="images/img_2.jpg" alt="Free Website Template by Free-Template.co" class="img-fluid">
              </div>
              <div class="listing-item-content">
                <a href="listings-single.html" class="bookmark"><span class="icon-heart"></span></a>
                <a class="px-3 mb-3 category" href="#">Tipe 2</a>
                <h2 class="mb-1"><a href="listings-single.html">Tempat Tidur<br>Lemari</a></h2>
                <span class="address">Weda, Halmahera Tengah</span>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/rangeslider.min.js"></script>
  

  <script src="js/typed.js"></script>
            <script>
            var typed = new Typed('.typed-words', {
            strings: ["Attractions"," Events"," Hotels", " Restaurants"],
            typeSpeed: 80,
            backSpeed: 80,
            backDelay: 4000,
            startDelay: 1000,
            loop: true,
            showCursor: true
            });
            </script>

  <script src="js/main.js"></script>
  
  </body>
</html>